package com.cathode.yymealchk.mealview;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cathode.yymealchk.MainActivity;
import com.cathode.yymealchk.R;
import com.cathode.yymealchk.thread.mealthread;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class mealAdapter extends RecyclerView.Adapter<mealViewHolder> {

    private int NETWORK_WIFI            = 1;
    private int NETWORK_MOBILE          = 2;
    private int NETWORK_NOT_AVAILABLE   = 0;

    public static mealAdapter mContext;

    public static final String TAG=mealAdapter.class.getSimpleName();

    public mealthread thread;
    public Boolean lunch;
    public Boolean network=false;

    public int mode;
    public String strsql;
    public String []mealarry;
    public String []calarry;
    public String []dinnerarry;
    public String []dinnercalarry;
    public String []date;
    public List<mealItem> items;


   public mealAdapter(Boolean lunch,int mode) throws InterruptedException {
       super();

       thread = new mealthread();
       this.mode=mode;
       this.lunch=lunch;
       if (MainActivity.mMaincon.readfirst()){
           Log.d("APP First","First Excute");

           if(MainActivity.mMaincon.getOnline()!=0){
               thread.infochk=false;
               network=true;
                                                                                                /* 첫실행시 : First excute, Second excute 인 이유는 어답터 객체가 두번 생성되기때문*/
               MainActivity.mMaincon.savefirst();}

           loadall();
       }else{Log.d("APP First","Second++  Excute");

       loadall();
       }






   }


    public void loadall() throws InterruptedException {
        thread.setPriority(Thread.MAX_PRIORITY);

        this.mContext = this;


        thread.start();
        thread.mode=mode;

        thread.join();

        Log.d("Refresh",Integer.toString(thread.mode));




        items = new ArrayList<>();



        adddata();






    }

        public void adddata(){
             /*배열 제어*/
            for (int i = 0; i < 5; i++){


                if (dinnerarry[i].equals(" ")||dinnerarry[i].equals("null")){
                    dinnerarry[i]="급식이 없습니다\n\n";
                }

                if (mealarry[i].equals(" ")){
                    mealarry[i]="급식이 없습니다\n\n";
                }
                if (dinnercalarry[i].isEmpty()||dinnercalarry[i].equals("null")){
                    dinnercalarry[i]="0";
                }
                if (calarry[i].isEmpty()){
                    calarry[i]="0";
                }

            }



    /*리스트뷰에 항목 추가.*/
            if (this.lunch) {  // false 일때 저녁 true 일때 점심
                for (int i = 0; i <= 4; i++) {
                    items.add(new mealItem(mealarry[i], calarry[i] + "Kcal", date[i]));
                }
            }else{
                for (int i = 0; i <= 4; i++) {
                    items.add(new mealItem(dinnerarry[i], dinnercalarry[i] + "Kcal", date[i]));
                }

            }

        }



    @Override
    public mealViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.mealitem,viewGroup,false);
        return new mealViewHolder(view);
    }

    @Override
    public void onBindViewHolder(mealViewHolder mealViewHolder, int i) {
        final mealItem item=items.get(i);
            mealViewHolder.meal.setText(item.getMeal());
            mealViewHolder.cal.setText(item.getCal());
            mealViewHolder.date.setText(item.getDate());
    }

    @Override
    public int getItemCount() {
        return items.size();
    }



}
