package com.cathode.yymealchk.mealview;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cathode.yymealchk.R;
import com.gc.materialdesign.views.ButtonFlat;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class mealViewHolder extends RecyclerView.ViewHolder {
  ButtonFlat sharebtn;
   TextView meal;
   TextView cal;
   TextView date;
    public mealViewHolder(final View itemView) {
        super(itemView);
        meal=(TextView)itemView.findViewById(R.id.meallist);
        cal=(TextView)itemView.findViewById(R.id.callist);
        date=(TextView)itemView.findViewById(R.id.datetxt);
        sharebtn=(ButtonFlat)itemView.findViewById(R.id.buttonshare);


        sharebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context =itemView.getContext();
               int position=getPosition();

                    String reformmeal;

                char []txt =meal.getText().toString().toCharArray();
                for(int i=0;i<txt.length;i++){

                    for(int j=9312;j<=9331;j++){
                        if(txt[i]==j){
                            txt[i]=' ';
                            break;
                        }
                    }


                }

                reformmeal=new String(txt,0,txt.length);

                Intent msg = new Intent(Intent.ACTION_SEND);
                msg.addCategory(Intent.CATEGORY_DEFAULT);
                msg.putExtra(Intent.EXTRA_SUBJECT, date.getText().toString() + "\n\n\n#그날의급식\n\n");
                msg.putExtra(Intent.EXTRA_TEXT,reformmeal+"\n\n칼로리:"+cal.getText().toString());
                msg.setType("text/plain");
               context.startActivity(msg);
            }
        });
    }
}
