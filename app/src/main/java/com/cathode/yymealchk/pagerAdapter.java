package com.cathode.yymealchk;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class pagerAdapter extends FragmentStatePagerAdapter {

    public pagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position){
        if (position==0){
            return "점심";
        }else if(position==1){
            return "저녁";
        }else {      return null;}

    }

    @Override
    public Fragment getItem(int position) {
        switch (position){
            case 0:
                return new lunchfrag();
            case 1:
                return new dinnerfrag();
            default:
                return null;
        }


    }

    @Override
    public int getCount() {
        return 2;
    }
}
