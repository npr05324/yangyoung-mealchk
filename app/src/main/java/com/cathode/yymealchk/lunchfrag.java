package com.cathode.yymealchk;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.cathode.yymealchk.mealview.mealAdapter;
import com.cathode.yymealchk.thread.mealthread;

/**
 * Created by lemmondev on 2015-04-10.
 */
public class lunchfrag extends Fragment {


    public static lunchfrag mContext;




    private int NETWORK_WIFI            = 1;
    private int NETWORK_MOBILE          = 2;
    private int NETWORK_NOT_AVAILABLE   = 0;

    private static final float MAX_SWIPE_DISTANCE_FACTOR = .6f;
    private static final int REFRESH_TRIGGER_DISTANCE = 120;


    RecyclerView meallist;
    public View view;
    mealAdapter ad;

    SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        view = inflater.inflate(R.layout.lunchfrag,container,false);

        meallist= (RecyclerView) view.findViewById(R.id.mealrecy);


        mSwipeRefreshLayout=(SwipeRefreshLayout)view.findViewById(R.id.activity_swipe_reload_main);





        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        try {
                            reloadmeal();
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }




                    }
                });
        try {
            ad = new mealAdapter(true,0);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        final LinearLayoutManager layoutParams = new LinearLayoutManager(this.getActivity());

         meallist.setItemAnimator(new DefaultItemAnimator());
         meallist.setLayoutManager(layoutParams);
         meallist.setOnScrollListener(new RecyclerView.OnScrollListener() {
             @Override
             public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
             }

             @Override
             public void onScrolled(RecyclerView view, int dx, int dy) {
                 mSwipeRefreshLayout.setEnabled(layoutParams.findFirstCompletelyVisibleItemPosition() == 0); // 최상단에서만 새로고침 되도록 설정.
             }
         });
            meallist.setAdapter(ad);

        this.mContext=this;

        return view;


    }




    public void reloadmeal() throws InterruptedException {


            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {



                    if (getOnline()==0){
                        Toast.makeText(lunchfrag.this.getActivity().getBaseContext(),"네트워크 연결을 확인해 주세요!!",Toast.LENGTH_LONG).show();
                        mSwipeRefreshLayout.setRefreshing(false);
                    }else{
                    new getdata().execute();

                    }



                }
            },4000);







    }
   public class getdata extends AsyncTask<Void,Void,Void> {

        @Override
        protected void onPreExecute() {

            Log.d("AsyncTask","Start Task!!");
            super.onPreExecute();



        }


       @Override
       protected Void doInBackground(Void... params) {


                    Log.d("AsyncTask","Start Refresh adapter!!");
                    ad.thread = new mealthread();
                    ad.thread.setPriority(Thread.MAX_PRIORITY);

                    ad.thread.mode = 1;
                    ad.thread.start();

                    try {
                        ad.thread.join();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    Log.d("AsyncTask","End Refresh");




           return null;
       }

        @Override
       protected void onPostExecute(Void result) {

            Log.d("AsyncTask","set Adapter");
            try {
                ad=new mealAdapter(true,0);  //점심 새로고침
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            meallist.setAdapter(ad);

         dinnerfrag.mContextdinner.refreshdinner(); //저녁 새로고침


           mSwipeRefreshLayout.setRefreshing(false);
            Log.d("AsyncTask","End set Adapter");
            Toast.makeText(lunchfrag.this.getActivity().getBaseContext(),"새로 고침이 완료되었어요!!",Toast.LENGTH_LONG).show();

       }




    }

    private int getOnline(){
        try{
            ConnectivityManager conMan =(ConnectivityManager)MainActivity.mMaincon.getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo.State wifi =conMan.getNetworkInfo(1).getState();  //WIFI
            if (wifi==NetworkInfo.State.CONNECTED ||wifi== NetworkInfo.State.CONNECTING) return NETWORK_WIFI;

            NetworkInfo.State mobile= conMan.getNetworkInfo(0).getState();
            if (mobile==NetworkInfo.State.CONNECTED||mobile==NetworkInfo.State.CONNECTING) return  NETWORK_MOBILE;




        }catch (NullPointerException e){
            return NETWORK_NOT_AVAILABLE;

        }
        return NETWORK_NOT_AVAILABLE;

    }



}

