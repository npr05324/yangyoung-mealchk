package com.cathode.yymealchk;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by lemmondev on 2015-04-11.
 */
public class mealDB extends SQLiteOpenHelper {

    public mealDB(Context context){
        super(context,"mealcache",null,1);


    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE `mealist` (" +
                "`date` VARCHAR( 20 ) NOT NULL ," +
                "`list` VARCHAR( 512 ) NOT NULL ," +
                "`kcal` VARCHAR( 16 ) NOT NULL ," +
                "`type` VARCHAR( 16 ) NOT NULL" +
                ")");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
           db.execSQL("drop table if exists meallist");
            onCreate(db);
    }
}
