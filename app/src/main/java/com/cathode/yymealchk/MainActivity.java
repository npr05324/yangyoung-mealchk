package com.cathode.yymealchk;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import it.neokree.materialtabs.MaterialTab;
import it.neokree.materialtabs.MaterialTabHost;
import it.neokree.materialtabs.MaterialTabListener;
import me.drakeet.materialdialog.MaterialDialog;


public class MainActivity extends ActionBarActivity implements MaterialTabListener {

    private int NETWORK_WIFI            = 1;
    private int NETWORK_MOBILE          = 2;
    private int NETWORK_NOT_AVAILABLE   = 0;

    public MaterialDialog mDialog = null;

    public static MainActivity mMaincon;
    MaterialTabHost tabHost;
    ViewPager pager;
    pagerAdapter adapter;
    public mealDB mDbhelper;

    SharedPreferences mPref = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mPref = getSharedPreferences("firstchk",MODE_PRIVATE);

        if (readfirst()){

            if (getOnline()==0){
                Toast.makeText(this,"네트워크 연결을 확인 해주세요!",Toast.LENGTH_LONG).show();


            finish();}


        }

        Toolbar toolbar = (android.support.v7.widget.Toolbar) this.findViewById(R.id.toolbar);
        this.setSupportActionBar(toolbar);
        tabHost = (MaterialTabHost) this.findViewById(R.id.tabhost);
        pager = (ViewPager) this.findViewById(R.id.viewpager);
        toolbar.setTitleTextColor(Color.WHITE);


        adapter = new pagerAdapter(getSupportFragmentManager());
        pager.setAdapter(adapter);
        pager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                tabHost.setSelectedNavigationItem(position);
            }
        });

        for (int i = 0; i < 2; i++) {
            tabHost.addTab(tabHost.newTab()
                            .setText(adapter.getPageTitle(i))
                            .setTabListener(this)
            );

        }

        mDbhelper = new mealDB(this);




        if (readfirst()){
            mDialog= new MaterialDialog(this)
            .setTitle(R.string.dialogname)
            .setMessage(R.string.dialogbody)
            .setPositiveButton("확인",new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mDialog.dismiss();
                }
            });
            mDialog.show();

        }


        this.mMaincon = this;


    }


    public void savefirst(){
        SharedPreferences.Editor editor = mPref.edit();

        editor.putBoolean("chk",false);
        editor.commit();
    }

    public Boolean readfirst(){
        Boolean chk = mPref.getBoolean("chk",true);
        return chk;
    }






    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_info) {
            mDialog= new MaterialDialog(this)
                    .setTitle(R.string.dialogname)
                    .setMessage(R.string.dialogbody)
                    .setPositiveButton("확인",new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mDialog.dismiss();
                        }
                    });
            mDialog.show();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabSelected(MaterialTab materialTab) {
        pager.setCurrentItem(materialTab.getPosition());
    }

    @Override
    public void onTabReselected(MaterialTab materialTab) {

    }

    @Override
    public void onTabUnselected(MaterialTab materialTab) {

    }
   public int getOnline(){
        try{
            ConnectivityManager conMan =(ConnectivityManager)MainActivity.mMaincon.getApplication().getSystemService(Context.CONNECTIVITY_SERVICE);

            NetworkInfo.State wifi =conMan.getNetworkInfo(1).getState();  //WIFI
            if (wifi==NetworkInfo.State.CONNECTED ||wifi== NetworkInfo.State.CONNECTING) return NETWORK_WIFI;

            NetworkInfo.State mobile= conMan.getNetworkInfo(0).getState();
            if (mobile==NetworkInfo.State.CONNECTED||mobile==NetworkInfo.State.CONNECTING) return  NETWORK_MOBILE;




        }catch (NullPointerException e){
            return NETWORK_NOT_AVAILABLE;

        }
        return NETWORK_NOT_AVAILABLE;

    }


}
